###############################################################
####################### DIY Led pannel ########################
###############################################################


* Specification :

For my project, I need a powerful LED pannel from 13000 to 17000 lumens.
I choose the Samsung LM301B. Each LED consumes between 2,7V (45 mA) and 2,9V
(120 mA). Samsung made all tests at 2,75V and 65mA.
(https://cdn.samsung.com/led/file/resource/2020/03/Data_Sheet_LM301B_CRI80_Rev.10.0.pdf).

I draw PCB on Easy EDA. There is 10 Rows of 10 parrallele LEDs.
For the pannel I have a 28V-5A alimentation.
Because I use 28V alimentation, each row consume 2,8V * 10 LEDs and 95mA (cf. datasheet).
Because There is 10 rows, 1 pannel consume 95mA * 10 = 950mA.

For 2,75V, each LED produce from 34 to 40 lm depending on the temperature and the exact
reference of the LED. For 1 pannel : (34-40 lm) * 100 LEDs = (3400-4000) lm.
My alimentation can turn on 5 pannels max. The Samsung datasheet show the diagram of the
evolution of luminous flux (%) according to the current and we can see for 95mA,
the luminous flux is close to 130% of the normal flux.
5 pannels represente : 5pannels * 100LEDs * (34-40 lm) * 1,3% = 22100-26000 lm, it's to much.
4 pannels represente : 4 * 100 * (34-40) * 1,3 = 17680-20800 lm, it's to much
3 pannels represente : 3 * 100 * (34-40) * 1,3 = 13260-15600 lm, perfect.
So 3 pannels is the best option for me.
The installation consume 0,95A * 3pannels = 2,85A so 2,85A * 28V = 79,8W.

The LED repartition for the first model is :
- 40 5000K
- 10 4000K
- 10 3000K
- 40 2700K
lm total with 28V : 3 * (40 * 34 + 10 * 36 + 38 * 10 + 40 * 38) * 1,3 = 14118lm

The LED repartition for the second model is :
- 20 5000K
- 10 4000K
- 10 3000K
- 60 2700K
lm total with 28V : 3 * (60 * 34 + 10 * 36 + 38 * 10 + 20 * 38) * 1,35 = 14430lm

The LED repartition for the hybrid model is :
- 30 5000K
- 10 4000K
- 10 3000K
- 50 2700K
lm total with 28V : 3 * (50 * 34 + 10 * 36 + 38 * 10 + 30 * 38) * 1,35 = 13962lm


* PCB Components :

PCB color : white
size : 174x189mm
holes diameter : 6mm
connector : C319133 (LCSC)
LED ref (Digikey):
					- 2700K : SPMWHD32AMD5XAW3S0 / SPMWHD32AMD5XAW0S0 (34lm)
(https://ww	w.digikey.fr/product-detail/fr/samsung-semiconductor-inc/SPMWHD32AMD5XAW0S0/1510-2374-6-ND/8568527)
(https://www.digikey.fr/product-detail/fr/samsung-semiconductor-inc/SPMWHD32AMD5XAW3S0/1510-2373-6-ND/8568526)

					- 3000K : SPMWHD32AMD5XAV0S0 (36lm)
(https://www.digikey.fr/product-detail/fr/samsung-semiconductor-inc/SPMWHD32AMD5XAV0S0/1510-2376-6-ND/8568529)

					- 4000K : SPMWHD32AMD5XAT3S0 (38lm)
(https://www.digikey.fr/product-detail/fr/samsung-semiconductor-inc/SPMWHD32AMD5XAT3S0/1510-2379-6-ND/8568532)

					- 5000K : SPMWHD32AMD5XAR0S0 (38lm)
(https://www.digikey.fr/product-detail/fr/samsung-semiconductor-inc/SPMWHD32AMD5XAR0S0/1510-2382-6-ND/8568535)
(https://www.digikey.fr/product-detail/fr/samsung-semiconductor-inc/SPMWHD32AMD5XAR0S0/1510-2382-1-ND/8568398)


Copyright Lyes Hamroun.
